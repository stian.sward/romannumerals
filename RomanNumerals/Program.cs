﻿using System;

namespace RomanNumerals
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Enter a number, or write 'test' to run an automatic sequence: ");
            string input = Console.ReadLine();
            if (input == "test")
            {
                for (int i = 1; i <= 4000; i++)
                {
                    input = Roman.To(i);
                    Console.WriteLine(i + " = " + input + " = " + Roman.From(input));
                }
            }
            else
            {
                Console.WriteLine(Roman.To(input));
            }
        }
    }
}
