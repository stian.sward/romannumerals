﻿using System;

namespace RomanNumerals
{
    class Roman
    {
        private static char[] romanLetters = new char[] { 'M', 'D', 'C', 'L', 'X', 'V', 'I' };
        private static int[] romanValues = new int[] { 1000, 500, 100, 50, 10, 5, 1 };

        public static string To(int arabic)
        {
            // Add an 'M' for each thousand
            string res = new string('M', arabic / 1000);
            arabic -= res.Length * 1000;

            for (int i = 2; i < romanLetters.Length; i += 2)
            {
                if (arabic / romanValues[i] == 9)
                {
                    res += romanLetters[i].ToString() + romanLetters[i - 2].ToString();
                    arabic -= romanValues[i] * 9;
                }
                else if (arabic / romanValues[i] >= 5)
                {
                    res += romanLetters[i - 1] + new string(romanLetters[i], (arabic / romanValues[i] - 5));
                    arabic -= romanValues[i] * (arabic / romanValues[i]);
                }
                else if (arabic / romanValues[i] == 4)
                {
                    res += romanLetters[i].ToString() + romanLetters[i - 1].ToString();
                    arabic -= romanValues[i] * 4;
                }
                else
                {
                    res += new string(romanLetters[i], arabic / romanValues[i]);
                    arabic -= romanValues[i] * (arabic / romanValues[i]);
                }
            }

            return res;
        }

        public static int From(string roman)
        {
            int res = 0;
            int currentLetterValue, nextLetterValue;

            for (int i = 0; i < roman.Length; i++)
            {
                currentLetterValue = romanValues[Array.IndexOf(romanLetters, roman[i])];
                nextLetterValue = (i < roman.Length - 1) ? romanValues[Array.IndexOf(romanLetters, roman[i + 1])] : 0;
                if (nextLetterValue > currentLetterValue)
                {
                    res += nextLetterValue - currentLetterValue;
                    i++;
                }
                else
                {
                    res += currentLetterValue;
                }
            }

            return res;
        }

        /**
         * Overridden To method taking a string parameter and handling integer parsing
         */
        public static string To(string input)
        {
            try
            {
                return To(int.Parse(input));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return null;
        }
    }
}
